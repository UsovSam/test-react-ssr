/**
 * Created by Sam.
 */
const webpack = require('webpack');
const path = require('path');

module.exports = {
    entry: [
        'webpack/hot/dev-server?http://localhost:8080',
        'webpack-hot-middleware/client',
        './src/index'
    ],
    output: {
        path: path.join(__dirname, 'static'),
        filename: 'bundle.js',
        publicPath: '/static/'
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('development')
        })
    ],
    module: {
        rules: [
            {test: /\.js$/, exclude: /node_modules/, use: {loader: 'babel-loader', options: {presets: ['env', 'react', 'stage-2']}}},
            {test: /\.jsx?$/, exclude: /node_modules/, use: {loader: 'babel-loader', options: {presets: ['env', 'react', 'stage-2']}}},
            {test: /\.css$/, use: [ 'style-loader', 'css-loader' ] }
        ]
    }/*,
    devServer: {
        hot: true,
        contentBase: path.resolve(__dirname, 'dist'),
        publicPath: '/'
    }*/
};
