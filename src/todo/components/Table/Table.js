import React, {Component} from 'react';
import HeaderTable from './HeaderTable';
import BodyTable from './BodyTable';
import './TableStyle.css'

const data = [
    {
        'price': 103,
        'isAsk': true
    },
    {
        'price': 102,
        'isAsk': true
    },
    {
        'price': 101,
        'isAsk': true
    },
    {
        'price': 99,
        'isAsk': false
    },
    {
        'price': 98,
        'isAsk': false
    },
    {
        'price': 97,
        'isAsk': false
    }
];

class Table extends Component {

    constructor(props) {
        super(props);
        this.state = {list: data};
    }

    render() {
        return (
            <table className="tableStyle">
                <HeaderTable />
                <BodyTable list={this.state.list}/>
            </table>
        );
    }

}

export default Table;
