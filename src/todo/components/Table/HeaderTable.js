import React, {Component} from 'react';

class HeaderTable extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <thead>
                    <tr>
                        <th>Заявки на покупку(bid)</th>
                        <th>Цена</th>
                        <th>Заявки на продажу(ask)</th>
                    </tr>
            </thead>
        );
    }

}

export default HeaderTable;
