import React, {Component} from 'react';

const randomNumber = () => Math.round(Math.random() * 1000);

class BodyTable extends Component {

    constructor(props) {
        super(props);
        let askData = this.props.list
            .filter(it => it.isAsk)
            .sort((it1, it2) => it1.price < it2.price);

        let bidData = this.props.list
            .filter(it => !it.isAsk)
            .sort((it1, it2) => it1.price < it2.price);




        this.state = {
            list: [...askData, {price: askData[askData.length - 1].price - 1, spred: true}, ...bidData],
            indexSpred: askData.length
        }
    }

    componentWillMount() {
        this.startTimer();
    }


    render() {
        return (
            <tbody>
            {
                this.state.list.map(item => (
                    <tr key={item.price}>
                        <td>{(!item.spred && !item.isAsk) ? randomNumber() :  ''}</td>
                        <td>{item.price}</td>
                        <td>{(!item.spred && item.isAsk) ? randomNumber() : ''}</td>
                    </tr>
                ))
            }
            </tbody>
        );
    }

    startTimer() {
        const sendMessageTimer = setInterval(() => {
            this.changeSpred();
        }, 5000);
        this.setState({ sendMessageTimer });
    }

    changeSpred() {
        let list = this.state.list;
        let buf = [];
        const directionUp = Math.random() > 0.5;

        if (directionUp) {
            //add new first el ask
            buf.push({
                'price': list[0].price + 1,
                'isAsk': true
            });
            //add all ask
            buf.push(...list.slice(0,this.state.indexSpred - 1));

            //change pos of spred
            buf.push({
                'price': buf[buf.length - 1].price - 1,
                'spred': true
            });

            //add new first el bid
            buf.push({
                'price': buf[buf.length - 1].price - 1,
                'isAsk': false
            });
            //add all bid
            buf.push(...list.slice(this.state.indexSpred + 1, list.length - 1));

        } else {
            //add all ask without first
            buf.push(...list.slice(1,this.state.indexSpred));

            //add new first el ask
            buf.push({
                'price': buf[buf.length - 1].price - 1,
                'isAsk': true
            });

            //change pos of spred
            buf.push({
                'price': buf[buf.length - 1].price - 1,
                'spred': true
            });

            //add all bid without first
            buf.push(...list.slice(this.state.indexSpred + 2, list.length));

            //add new first el bid
            buf.push({
                'price': buf[buf.length - 1].price - 1,
                'isAsk': false
            });

        }
        this.setState({ list: buf });
    }

    /*clearMessageTimer({ forceClear = false }) {
        const { sendMessageSeconds, sendMessageTimer } = this.state;
        // Удалим таймер, если он существует и оставшееся время стало <=0 или его надо удалить явным образом
        if (sendMessageTimer !== 0 && (forceClear || sendMessageSeconds <= 0)) {
            clearInterval(sendMessageTimer);
            this.setState({
                sendMessageSeconds: 0,
                sendMessageTimer: 0,
            });
        }
    }*/
}

export default BodyTable;
