const app = require('./app.js');
const port = 3000;

app.listen(port, error => {
    if (error) {
        console.error(error);
    } else {
        console.info(`Listening on port ${port}.`);
    }
});
